Summary: Library for the Portable Network Graphics Format (PNG)
Name: libpng12
Version: 1.2.57
Release: 13
License: Zlib
URL: http://www.libpng.org/pub/png/

Source0: https://sourceforge.net/projects/libpng/files/libpng12/older-releases/%{version}/libpng-%{version}.tar.xz
Patch6000: CVE-2011-3026.patch
Patch6001: CVE-2013-7353-1.patch
Patch6002: CVE-2013-7353-2.patch
Patch6003: CVE-2013-7353-3.patch
Patch6004: CVE-2013-7354-1.patch
Patch6005: CVE-2013-7354-2.patch
Patch6006: CVE-2017-12652.patch

BuildRequires: gcc pkgconfig zlib-devel

%description
libpng is the official reference library for the Portable Network
Graphics format (PNG). This package provides an older version of the libpng.

%package devel
Summary: Development files for libpng12
Requires: %{name}%{?_isa} = %{version}-%{release}
Requires: zlib-devel%{?_isa}

%description devel
The libpng12-devel package contains header and help files for developing
programs using libpng12.

%prep
%autosetup -n libpng-%{version} -p1

%build
%configure --disable-static --without-libpng-compat
%make_build

%install
%make_install
%delete_la
find %{buildroot} -type l -name "*.la" -delete

%check
make check

%files
%{!?_licensedir:%global license %%doc}
%license LICENSE
%doc libpng-%{version}.txt README TODO CHANGES
%{_libdir}/libpng12.so.0*
%exclude %{_mandir}/man5/*

%files devel
%{_bindir}/libpng12-config
%{_includedir}/libpng12/
%{_libdir}/libpng12.so
%{_libdir}/pkgconfig/libpng12.pc
%exclude %{_bindir}/libpng-config
%exclude %{_includedir}/{png,pngconf}.h
%exclude %{_libdir}/libpng.so
%exclude %{_libdir}/pkgconfig/libpng.pc
%exclude %{_mandir}/man3/{libpng,libpngpf}.3*

%changelog
* Wed Aug 04 2021 chenyanpanHW <chenyanpan@huawei.com> - 1.2.57-13
- DESC: delete BuildRequires gdb

* Mon Jun 29 2020 Guoshuai Sun <sunguoshuai@huawei.com> - 1.2.57-12
- Fix CVE-2013-7353 CVE-2013-7354 CVE-2017-12652
  And we have check CVE-2014-9495 CVE-2011-3045 CVE-2008-3964 CVE-2015-8126 CVE-2015-0973 alreadyi been fixed before.
  And CVE-2016-3751 CVE-2013-6954 didn'd involved.

* Fri May 22 2020 cuibaobao <cuibaobao1@huawei.com> - 1.2.57-11
- Type:cves
- ID:CVE-2011-3026
- SUG:restart
- DESC:fix CVE-2011-3026

* Tue Nov 19 2019 mengxian <mengxian@huawei.com> - 1.2.57-10
- Package init
